<?php
/************************************************************************
 * Footer File
 *************************************************************************/

do_action('wbc907_before_footer');

global $wbc907_data;

$wbc907_footer_columns = (isset($wbc907_data['opts-footer']) && is_numeric($wbc907_data['opts-footer'])) ? $wbc907_data['opts-footer'] : 4;

$footer_class = ($wbc907_footer_columns == 4) ? 'col-sm-3' : 'col-sm-4';

//Check for enabled states of footer/widget area/copyright area
$wbc907_footer_enable = apply_filters('wbc907_footer_enable', true);
$wbc907_widget_area_enable = apply_filters('wbc907_widget_area_enable', true);
$wbc907_copy_area_enable = apply_filters('wbc907_copy_area_enable', true);

//if footer option true
if ($wbc907_footer_enable):
    ?>
    <!-- Begin Footer -->
    <footer class="main-footer">

        <?php if ($wbc907_widget_area_enable): ?>

            <div class="widgets-area">
                <div class="container">
                    <div class="row">


                        <div class="<?php echo esc_attr($footer_class); ?>">

                            <?php if (is_active_sidebar('footer-1')) {
                                dynamic_sidebar('footer-1');
                            } else {
                                ?>
                                <div class="widget">
                                    <h4 class="widget-title">
                                        <?php esc_html_e('Archives', 'ninezeroseven'); ?>
                                    </h4>
                                    <ul>
                                        <?php wp_get_archives(); ?>
                                    </ul>
                                </div>
                            <?php } ?>

                        </div>

                        <div class="<?php echo esc_attr($footer_class); ?>">

                            <?php if (is_active_sidebar('footer-2')) {
                                dynamic_sidebar('footer-2');
                            } else {
                                ?>
                                <div class="widget  widget_pages">
                                    <h4 class="widget-title">
                                        <?php esc_html_e('Pages', 'ninezeroseven'); ?>
                                    </h4>

                                    <ul>
                                        <?php wp_list_pages('sort_column=menu_order&title_li='); ?>
                                    </ul>

                                </div>
                            <?php } ?>

                        </div>

                        <div class="<?php echo esc_attr($footer_class); ?>">

                            <?php if (is_active_sidebar('footer-3')) {
                                dynamic_sidebar('footer-3');
                            } else {
                                ?>
                                <div class="widget">
                                    <h4 class="widget-title">
                                        <?php esc_html_e('Categories', 'ninezeroseven'); ?>
                                    </h4>
                                    <ul class="categories">
                                        <?php wp_list_categories('title_li='); ?>
                                    </ul>
                                </div>
                            <?php } ?>
                        </div>


                        <?php if ($wbc907_footer_columns == 4): ?>
                            <div class="<?php echo esc_attr($footer_class); ?>">

                                <?php if (is_active_sidebar('footer-4')) {
                                    dynamic_sidebar('footer-4');
                                } else {
                                    ?>
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            <?php esc_html_e('Meta', 'ninezeroseven'); ?>
                                        </h4>
                                        <ul>
                                            <?php wp_register(); ?>
                                            <li><?php wp_loginout(); ?></li>
                                            <?php wp_meta(); ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>

                        <?php endif; ?>


                    </div>
                </div> <!-- ./container -->
            </div>
        <?php endif; //$wbc907_widget_area_enable
        ?>

        <?php if ($wbc907_copy_area_enable): ?>

            <div class="bottom-band">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 copy-info">

                            <?php

                            if (isset($wbc907_data['opts-footer-credit']) && !empty($wbc907_data['opts-footer-credit'])) {

                                echo wp_kses_post(do_shortcode($wbc907_data['opts-footer-credit']));

                            } else {
                                $footer_text = sprintf(__('&copy; <a href="%1s">%2s</a> All Rights Reserved %3s - Powered By <a href="http://wordpress.org">WordPress</a>', 'ninezeroseven'),
                                    home_url(),
                                    get_bloginfo('name'),
                                    date('Y'));

                                echo wp_kses_post($footer_text);
                            }

                            ?>
                        </div>

                        <div class="col-sm-6 extra-info">
                            <?php

                            do_action('before_extra_info');

                            wp_nav_menu(array(
                                'container' => 'nav',
                                'container_class' => 'footer-menu',
                                'container_id' => 'wbc9-footer',
                                'menu' => apply_filters('wbc907_page_footer_menu', ''),
                                'menu_id' => 'footer-menu',
                                'menu_class' => 'wbc_footer_menu',
                                'theme_location' => 'wbc907-footer',
                                'fallback_cb' => ''
                            ));

                            do_action('after_extra_info');

                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; //$copy_area_enable
        ?>
    </footer>

<?php endif; // wbc907_footer_enable ?>

</div> <!-- ./page-wrapper -->

<!--<div class="box">
    <a class="button" href="#investment">Investment</a>
</div>-->
<?php

if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
}
if (!is_user_logged_in()) {
?>

<div id="login" class="overlay">
    <div class="popup">
        <div class="note text-center">
            <h3>Sign In</h3>
            <div class="alert alert-danger hidden" role="alert"></div>
        </div>
        <a class="close" href="#">&times;</a>
        <div class="content">
            <div class="register-login-form">
                <form name="loginform" id="loginFrm" action="<?php echo site_url('/wp-login.php'); ?>" method="post">
                    <div class="form-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 ">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="log" id="user_login"
                                               placeholder="Enter Your Username*" value=""/>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="pwd" id="user_pass"
                                               placeholder="Enter Your password*" value=""/>
                                    </div>
                                    <div class="pull-left">
                                        <input type="submit" class="custom-btn login-sub-btn" name="signin"
                                               value="Sign In"/>
                                        <p class="padd-top">Forget your password? <a
                                                    href="<?php echo get_bloginfo('url'); ?>/wp-login.php?action=lostpassword">
                                                Reset Password</a></p>
                                    </div>
                                    <?php if (!is_singular('wbc-portfolio')) {?>
                                    <div class="pull-right">
                                        Dont' have an account yet? <a href="#register"> Sign Up Now </a>
                                    </div>
                                    <?php }else{?>
                                        <div class="pull-right">
                                            Dont' have an account yet? <a href="<?php bloginfo('url');?>/#register"> Sign Up Now </a>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php if (!is_singular('wbc-portfolio')) {?>
<div id="register" class="overlay">
    <div class="popup">
        <div class="note text-center">
            <h3>Sign up</h3>
            <div class="alert alert-danger hidden" role="alert"></div>
            <div class="alert alert-success hidden"></div>
        </div>
        <a class="close" href="#">&times;</a>
        <div class="content">
            <div class="register-login-form">
                <form name="registerform" id="registerform"
                      action="<?php bloginfo('url'); ?>/wp-login.php?action=register" method="post"
                      novalidate="novalidate">
                    <div class="form-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 ">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="user_login"
                                               placeholder="Enter Your Name *" value=""/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="company_name"
                                               placeholder="Enter Your Company Name *" value=""/>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="user_email"
                                               placeholder="Enter Your Email*" value=""/>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="password"
                                               placeholder="Enter Your password*" value=""/>
                                    </div>

                                    <div class="pull-left">
                                        <!-- Google reCAPTCHA box -->
                                        <div class="g-recaptcha" data-sitekey="6LeMUq0UAAAAAAR72TJVIXdXZXCoFOqTgiv0R1ve"></div>
                                        <input type="submit" class="custom-btn sign-sub-btn" name="signup"
                                               value="Sign up"/>
                                    </div>

                                    <div class="pull-right">
                                        Already have an account? <a href="#login">Sign In Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php };?>
<?php };?>
<div id="investment" class="overlay">
    <div class="popup">
        <div class="note text-center">
            <h3>CONFIDENTIALITY AGREEMENT</h3>
            <div class="alert alert-danger hidden" role="alert"></div>
            <div class="alert alert-success hidden"></div>
        </div>
        <a class="close invest-close" href="#">&times;</a>
        <div class="content">
            <?php
            $pid = '';
            $agreement_text = '';
            if (is_singular('wbc-portfolio')) {
                global $post;
                $pid = $post->ID;
                $agreement_text = get_post_meta($post->ID, 'agreement_text', true);
                $agreement_text = str_replace("\'0a\'0a",'<br/>',$agreement_text);
                $agreement_text = str_replace(".\'0a\'0a1",'<br/>',$agreement_text);
                $agreement_text = str_replace(".\'0a\'0a1",'<br/>',$agreement_text);
                $agreement_text = str_replace(".\'0a\'0a2",'<br/>',$agreement_text);
                $agreement_text = str_replace(".\'0a\'0a3",'<br/>',$agreement_text);
                $agreement_text = str_replace(".\'0a\'0a4",'<br/>',$agreement_text);
                $agreement_text = str_replace(".\'0a\'0a\'0a",'<br/>',$agreement_text);
                $agreement_text = str_replace("\'0a",'<br/>',$agreement_text);

                $agreement_text = clean($agreement_text);
            }
            ?>
            <div class="register-login-form">
                <div class="agreement-box">
                    <div class="agreement-text">
                        <p><?php echo $agreement_text;?></p>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label><input id="agreecheck" name="agreecheck" type="checkbox" value="agree">I agree to terms & conditions</label>
                        </div>
                    </div>
                    <input type="submit" class="custom-btn agree-btn" name="agreebtn" value="Submit" disabled/>
                </div>

                <form class="form" id="register_content">
                    <div class="form-content">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 ">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="user_login2"
                                           placeholder="Enter Your Name *"
                                           value="<?php if (isset($current_user->ID) && !empty($current_user->ID)) {
                                               echo $current_user->display_name;
                                           } ?>"/>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="company_name2"
                                           placeholder="Enter Your Company Name *"
                                           value="<?php if (isset($current_user->ID) && !empty($current_user->ID)) {
                                               echo get_user_meta($current_user->ID, 'company_name', true);
                                           } ?>"/>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="user_email2"
                                           placeholder="Enter Your Email*"
                                           value="<?php if (isset($current_user->ID) && !empty($current_user->ID)) {
                                               echo $current_user->user_email;
                                           } ?>"/>
                                </div>
                                <?php if (!is_user_logged_in()) { ?>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label><input id="create_check" name="create_check" type="checkbox"
                                                          value="create">Create my account</label>
                                        </div>
                                    </div>

                                    <div class="form-group" id="create_pass">
                                        <input type="password" class="form-control" name="password2"
                                               placeholder="Enter Your password*" value=""/>
                                    </div>
                                    <br/>
                                    <div class="g-recaptcha" data-sitekey="6LeMUq0UAAAAAAR72TJVIXdXZXCoFOqTgiv0R1ve"></div>
                                <?php }; ?>
                                <div class="pull-left">
                                    <input type="submit" class="custom-btn sub-for-inv" name="signup" data-pid="<?php echo $pid; ?>" value="Submit"/>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<?php

do_action('wbc907_after_page_content');

wp_footer();
?>
</body>
</html>