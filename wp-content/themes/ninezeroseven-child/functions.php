<?php
/************************************************************************
 *
 * ====== Functions File ======
 *
 * You can place custom functions in this file, make sure to place the
 * functions below this comment block.
 *************************************************************************/
function theme_js() {
    wp_enqueue_script( 'custom_js', get_stylesheet_directory_uri() . '/assets/js/custom.js', array( 'jquery' ), '1.0', true );
    wp_localize_script( 'custom_js', 'js_var', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'homeurl' => trailingslashit(home_url()) ) );
}

add_action('wp_enqueue_scripts', 'theme_js');




function my_page_template_redirect()
{
    if( is_page( 'taycora-marketplace' ) && ! is_user_logged_in() )
    {
        wp_redirect( home_url() );
        die;
    }
}
add_action( 'template_redirect', 'my_page_template_redirect' );

function add_new_menu_item($items, $args) {
    if( $args->theme_location == 'wbc907-primary' ){
        if( !is_user_logged_in()) {
            $items .= '<li class="menu-item log-reg-pop">'
                . '<a href="#login">CLIENT LOGIN</a>'
                . '</li>';
        }
    }
    return $items;
}
add_filter('wp_nav_menu_items', 'add_new_menu_item', 10, 2);


function check_authentication(){
    $logname = sanitize_text_field($_POST['logname']);
    $pwd = sanitize_text_field($_POST['pwd']);
    $user = get_user_by( 'login', $logname );
    $user1 = get_user_by( 'email', $logname );

    $status = get_user_meta($user->ID,'status', true);
    if(empty($status)){
        $status = get_user_meta($user1->ID,'status', true);
    }

    if($status == 'Unapprove'){
        echo json_encode(array('status' => 3));
    }else {
        if ((!empty($user) || !empty($user1)) && (wp_check_password($pwd, $user->data->user_pass, $user->ID) || wp_check_password($pwd, $user1->data->user_pass, $user1->ID))) {
            echo json_encode(array('status' => 1));
        } else {
            echo json_encode(array('status' => 2));
        }
    }
    exit();
}

add_action('wp_ajax_nopriv_check_authentication', 'check_authentication');


function save_registration_data(){
    $user_login = sanitize_text_field($_POST['user_login']);
    $company_name = sanitize_text_field($_POST['company_name']);
    $user_email = sanitize_text_field($_POST['user_email']);
    $pwd = sanitize_text_field($_POST['pwd']);
    $g_recaptcha_response = $_POST['g_recaptcha_response'];
    if (!empty($user_login))
        $user_name = preg_replace('/\s+/', '', $user_login);

    $user_id = username_exists($user_name);
    if (!$user_id && email_exists($user_email) == false) {

        if(isset($g_recaptcha_response) && !empty($g_recaptcha_response)) {

            $secretKey = '6LeMUq0UAAAAAMlXEEJ8wzKJ0RLRqTXK_76epmNW';
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secretKey . '&response=' . $g_recaptcha_response);
            $responseData = json_decode($verifyResponse);
            if ($responseData->success) {

                $userdata = array(
                    'user_pass' => $pwd,
                    'user_login' => $user_name,
                    'user_email' => $user_email,
                    'display_name' => $user_login,
                    'role' => 'subscriber'
                );
                $user_id = wp_insert_user($userdata);
                if ($user_id) {
                    update_user_meta($user_id, 'company_name', $company_name);
                    update_user_meta($user_id, 'status', 'Unapprove');
                    //wp_set_current_user($user_id);
                    //wp_set_auth_cookie($user_id);
                    //$user = get_user_by('id', $user_id);
                    //do_action('wp_login', $user->user_login);
                    $to = 'kyle@taycora.com';
                    //$to = 'developer2@puredevs.com';

                    $subject = 'New User Registration';

                    $headers = array('Content-Type: text/html; charset=UTF-8');
                    $headers[] = 'From: Taycora <'.$user_email.'>';

                    $message = '<html><body>';
                    $message .= '<h1>Hi!</h1>';
                    $message .= '<p style="font-size:18px;">A new user registration request submitted. Please click <a href="'.get_admin_url().'user-edit.php?user_id='.$user_id.'&approve=true" target="_blank">here</a> to approve user.</p>';
                    $message .= '<p style="font-size:18px;"><strong>Name: </strong>'.$user_login.'</p>';
                    $message .= '<p style="font-size:18px;"><strong>Company Name: </strong>'.$company_name.'</p>';
                    $message .= '<p style="font-size:18px;"><strong>Email: </strong>'.$user_email.'</p>';
                    $message .= '<p style="font-size:18px;">Thank you</p>';
                    $message .= '</body></html>';

                    wp_mail( $to, $subject, $message, $headers );

                    echo json_encode(array('status' => 1));
                    exit();
                } else {
                    echo json_encode(array('status' => 2));
                    exit();
                }
            }else{
                echo json_encode(array('status' => 5));
                exit();
            }
        }else{
            echo json_encode(array('status' => 4));
            exit();
        }

    }else {
        echo json_encode(array('status' => 3));
        exit();
    }
}
add_action('wp_ajax_nopriv_save_registration_data', 'save_registration_data');

/*function custom_redirect_init() {
    if ( is_user_logged_in() && !current_user_can( 'administrator' ) ) {
        $url = get_bloginfo('url').'/taycora-marketplace/';
        wp_redirect( $url );
        exit;
    }
}
add_action( 'init', 'custom_redirect_init' );*/

function my_login_redirect( $redirect_to, $request, $user ) {
    if (isset($user->roles) && is_array($user->roles)) {
        if (in_array('subscriber', $user->roles)) {
            $redirect_to =  get_bloginfo('url').'/taycora-marketplace/';
        }
    }

    return $redirect_to;
}

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

function pa_user_list_pay_link( $actions, $user_object ) {
    if ( current_user_can( 'administrator', $user_object->ID ) ){
        $actions['view'] = '<a href="'.get_edit_user_link( $user_object->ID ).'">View</a>';
    }
    return $actions;
}

add_filter( 'user_row_actions', 'pa_user_list_pay_link', 10, 2 );

function custom_user_profile_fields( $user ) {
    $company_name = get_user_meta($user->ID,'company_name', true);
    $status = get_user_meta($user->ID,'status', true);
    if(!empty($company_name)):
        ?>
        <table class="form-table">
            <tr>
                <th>
                    <label for="companyname"><?php esc_html_e( 'Company Name' ); ?></label>
                </th>
                <td>
                    <span class="companyname"><?php echo get_user_meta($user->ID,'company_name', true); ?></span>
                </td>
            </tr>
        </table>
    <?php
    endif;
    if(!empty($status)):
        if($_GET['approve']) {
            update_user_meta($user->ID, 'status', 'Approve');
        }
        ?>
        <table class="form-table">
            <tr>
                <th>
                    <label for="status"><?php esc_html_e( 'Status' ); ?></label>
                </th>
                <td>
                    <span class="status"><?php echo get_user_meta($user->ID,'status', true); ?></span>
                </td>
            </tr>
        </table>
    <?php
    endif;
}
add_action( 'show_user_profile', 'custom_user_profile_fields', 10, 1 );
add_action( 'edit_user_profile', 'custom_user_profile_fields', 10, 1 );

function add_custom_meta_box()
{
    $screens = ['wbc-portfolio'];
    foreach ($screens as $screen) {
        add_meta_box(
            'document_url',
            'Document Url',
            'document_url_callback_func',
            $screen
        );
        add_meta_box(
            'agreement_text',
            'Agreement Text',
            'agreement_text_callback_func',
            $screen
        );
    }
}
add_action('add_meta_boxes', 'add_custom_meta_box');

function document_url_callback_func($post)
{
    $value = get_post_meta($post->ID, 'document_url', true);
    ?>
    <label for="document_field">Enter Url:</label>
    <input type="text" name="document_url" value="<?php if(!empty($value)){ echo $value;}?>" style="width: 70%">
    <?php
}

function agreement_text_callback_func($post)
{
    $agreement_text = get_post_meta($post->ID, 'agreement_text', true);
    $agreement_text = str_replace("\'0a\'0a",'<br/>',$agreement_text);
    $agreement_text = str_replace(".\'0a\'0a1",'<br/>',$agreement_text);
    $agreement_text = str_replace(".\'0a\'0a1",'<br/>',$agreement_text);
    $agreement_text = str_replace(".\'0a\'0a2",'<br/>',$agreement_text);
    $agreement_text = str_replace(".\'0a\'0a3",'<br/>',$agreement_text);
    $agreement_text = str_replace(".\'0a\'0a4",'<br/>',$agreement_text);
    $agreement_text = str_replace(".\'0a\'0a\'0a",'<br/>',$agreement_text);
    $agreement_text = str_replace("\'0a",'<br/>',$agreement_text);

    $agreement_text = clean($agreement_text);
    ?>
    <textarea id="agreement_text_id" name="agreement_text" rows="10" style="width: 100%"><?php if(!empty($agreement_text)){ echo $agreement_text;}?></textarea>
    <?php
}

function cus_save_postdata($post_id)
{
    if (array_key_exists('document_url', $_POST)) {
        update_post_meta(
            $post_id,
            'document_url',
            $_POST['document_url']
        );
    }
    if (array_key_exists('agreement_text', $_POST)) {
        update_post_meta(
            $post_id,
            'agreement_text',
            $_POST['agreement_text']
        );
    }
}
add_action('save_post', 'cus_save_postdata');

function send_email_with_agreement(){

    $user_login = sanitize_text_field($_POST['user_login']);
    $company_name = sanitize_text_field($_POST['company_name']);
    $user_email = sanitize_text_field($_POST['user_email']);
    $pid = sanitize_text_field($_POST['pid']);
    $post = get_post($pid);

    if (!empty($post)) {
        $document_url = get_post_meta($post->ID, 'document_url', true);
        if(!empty($document_url)){
            $to = $user_email;
            $subject = 'From Taycora Advisors re: ' . get_the_title($pid) . '.';

            $headers = array('Content-Type: text/html; charset=UTF-8');
            $headers[] = 'From: Taycora <kyle@taycora.com>';
            $headers[] = 'Cc: Kyle Taylor <kyle@taycora.com>';

            $message = '<html><body>';
            $message .= '<h1>Hi '.$user_login.'!</h1>';
            //$message .= '<p style="font-size:18px;">Please check the document from <a href="'.$document_url.'" target="_blank">here</a>.</p>';
            $message .= '<p style="font-size:18px;">Please click <a href="'.$document_url.'" target="_blank">here</a> to access deal files.</p>';
            $message .= '<p style="font-size:18px;">Thank you</p>';
            $message .= '</body></html>';

            wp_mail( $to, $subject, $message, $headers );

            echo json_encode(array('status' => 1));
            exit();
        }else{
            echo json_encode(array('status' => 3));
        }
    }else {
        echo json_encode(array('status' => 2));
        exit();
    }
}
add_action('wp_ajax_send_email_with_agreement', 'send_email_with_agreement');
add_action('wp_ajax_nopriv_send_email_with_agreement', 'send_email_with_agreement');


function send_email_with_agreement2(){

    $user_login = sanitize_text_field($_POST['user_login']);
    $company_name = sanitize_text_field($_POST['company_name']);
    $user_email = sanitize_text_field($_POST['user_email']);
    $pid = sanitize_text_field($_POST['pid']);
    $g_recaptcha_response = $_POST['g_recaptcha_response'];
    $post = get_post($pid);

    if (!empty($post)) {
        $document_url = get_post_meta($post->ID, 'document_url', true);
        if(!empty($document_url)){

            if(isset($g_recaptcha_response) && !empty($g_recaptcha_response)) {

                $secretKey = '6LeMUq0UAAAAAMlXEEJ8wzKJ0RLRqTXK_76epmNW';
                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secretKey . '&response=' . $g_recaptcha_response);
                $responseData = json_decode($verifyResponse);
                if ($responseData->success) {


                    $to = $user_email;
                    $subject = 'From Taycora Advisors re: ' . get_the_title($pid) . '.';

                    $headers = array('Content-Type: text/html; charset=UTF-8');
                    $headers[] = 'From: Taycora <kyle@taycora.com>';
                    $headers[] = 'Cc: Kyle Taylor <kyle@taycora.com>';

                    $message = '<html><body>';
                    $message .= '<h1>Hi ' . $user_login . '!</h1>';
                    //$message .= '<p style="font-size:18px;">Please check the document from <a href="' . $document_url . '" target="_blank">here</a>.</p>';
                    $message .= '<p style="font-size:18px;">Please click <a href="' . $document_url . '" target="_blank">here</a> to access deal files.</p>';
                    $message .= '<p style="font-size:18px;">Thank you</p>';
                    $message .= '</body></html>';

                    wp_mail($to, $subject, $message, $headers);

                    echo json_encode(array('status' => 1));
                    exit();
                }else{
                    echo json_encode(array('status' => 5));
                    exit();
                }
            }else{
                echo json_encode(array('status' => 4));
                exit();
            }
        }else{
            echo json_encode(array('status' => 3));
        }
    }else {
        echo json_encode(array('status' => 2));
        exit();
    }
}
add_action('wp_ajax_send_email_with_agreement2', 'send_email_with_agreement2');
add_action('wp_ajax_nopriv_send_email_with_agreement2', 'send_email_with_agreement2');


add_action('template_redirect', function (){
    if(isset($_GET['cmail'])){
        /*$to = 'yeasirbdtest1@gmail.com';
        $subject = 'The estimate';
        $body = 'The email body content';
        $headers = array('Content-Type: text/html; charset=UTF-8','From: Taycora &lt;support@taycora.com');

        wp_mail( $to, $subject, $body, $headers );*/
        $to = 'yeasirbdtest1@gmail.com';
        $subject = 'The Rora';
        $body = '<p>The email body content</p>';
        $headers = array('Content-Type: text/html; charset=UTF-8');

        wp_mail( $to, $subject, $body, $headers );
    }
});

function save_registration_data_send_email_with_agreement(){
    $user_login = sanitize_text_field($_POST['user_login']);
    $company_name = sanitize_text_field($_POST['company_name']);
    $user_email = sanitize_text_field($_POST['user_email']);
    $pid = sanitize_text_field($_POST['pid']);
    $post = get_post($pid);
    $pwd = sanitize_text_field($_POST['pwd']);
    $g_recaptcha_response = $_POST['g_recaptcha_response'];
    if (!empty($user_login))
        $user_name = preg_replace('/\s+/', '', $user_login);

    $user_id = username_exists($user_name);
    if (!$user_id && email_exists($user_email) == false) {
        if(isset($g_recaptcha_response) && !empty($g_recaptcha_response)) {

            $secretKey = '6LeMUq0UAAAAAMlXEEJ8wzKJ0RLRqTXK_76epmNW';
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secretKey . '&response=' . $g_recaptcha_response);
            $responseData = json_decode($verifyResponse);
            if ($responseData->success) {
                $userdata = array(
                    'user_pass' => $pwd,
                    'user_login' => $user_name,
                    'user_email' => $user_email,
                    'display_name' => $user_login,
                    'role' => 'subscriber'
                );
                $user_id = wp_insert_user($userdata);
                if ($user_id) {
                    update_user_meta($user_id, 'company_name', $company_name);
                    update_user_meta($user_id, 'status', 'Unapprove');
                    //wp_set_current_user($user_id);
                    //wp_set_auth_cookie($user_id);
                    //$user = get_user_by( 'id', $user_id );
                    //do_action( 'wp_login', $user->user_login );
                    $to1 = 'kyle@taycora.com';

                    $subject1 = 'New User Registration';

                    $headers1 = array('Content-Type: text/html; charset=UTF-8');
                    $headers1[] = 'From: Taycora <'.$user_email.'>';

                    $message1 = '<html><body>';
                    $message1 .= '<h1>Hi!</h1>';
                    $message1 .= '<p style="font-size:18px;">A new user registration request submitted. Please click <a href="'.get_admin_url().'user-edit.php?user_id='.$user_id.'&approve=true" target="_blank">here</a> to approve user.</p>';
                    $message1 .= '<p style="font-size:18px;"><strong>Name: </strong>'.$user_login.'</p>';
                    $message1 .= '<p style="font-size:18px;"><strong>Company Name: </strong>'.$company_name.'</p>';
                    $message1 .= '<p style="font-size:18px;"><strong>Email: </strong>'.$user_email.'</p>';
                    $message1 .= '<p style="font-size:18px;">Thank you</p>';
                    $message1 .= '</body></html>';

                    wp_mail( $to1, $subject1, $message1, $headers1 );
                    if (!empty($post)) {
                        $document_url = get_post_meta($post->ID, 'document_url', true);
                        if(!empty($document_url)){
                            $to = $user_email;
                            $subject = 'From Taycora Advisors re: ' . get_the_title($pid) . '.';


                            $headers = array('Content-Type: text/html; charset=UTF-8');
                            $headers[] = 'From: Taycora <kyle@taycora.com>';
                            $headers[] = 'Cc: Kyle Taylor <kyle@taycora.com>';

                            $message = '<html><body>';
                            $message .= '<h1>Hi '.$user_login.'!</h1>';
                            $message .= '<p style="font-size:18px;">Please click <a href="'.$document_url.'" target="_blank">here</a> to access deal files.</p>';
                            $message .= '<p style="font-size:18px;">Thank you</p>';
                            $message .= '</body></html>';
                            wp_mail( $to, $subject, $message, $headers );
                            echo json_encode(array('status' => 4));
                            exit();
                        }
                    }
                    echo json_encode(array('status' => 1));
                    exit();
                }else{
                    echo json_encode(array('status' => 2));
                    exit();
                }
            }else{
                echo json_encode(array('status' => 5));
                exit();
            }
        }else{
            echo json_encode(array('status' => 6));
            exit();
        }
    }else {
        echo json_encode(array('status' => 3));
        exit();
    }
}
add_action('wp_ajax_nopriv_save_registration_data_send_email_with_agreement', 'save_registration_data_send_email_with_agreement');


function clean($string) {
    return preg_replace('/[^A-Za-z0-9\-\s()\.\/“”,’:<br *\/?>]/', '<br/>', $string);
}

add_action( 'phpmailer_init', 'fix' );
function fix( $phpmailer ) {
    $phpmailer->Sender = $phpmailer->From;
}