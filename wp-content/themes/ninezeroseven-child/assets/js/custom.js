;(function ($, window, document, undefined) {
    var $win = $(window);
    var $doc = $(document);
    var $body = $('body');

    $doc.ready(function () {

        $('#create_pass').hide();
        $('#create_check').click(function() {
            if ($(this).is(":checked"))
            {
                $('#create_pass').show();
            }else{
                $('#create_pass').hide();
            }
        });

        var $agreebtn =  $('.agree-btn');
        $agreebtn.css('opacity','0.5');
        $('#agreecheck').click(function() {
            if ($(this).is(":checked"))
            {
                $agreebtn.removeAttr('disabled')
                $agreebtn.css('opacity','1');

            }else{
                $agreebtn.attr('disabled', 'true');
                $agreebtn.css('opacity','0.5');
            }
        });

           $('#register_content').hide();
              $agreebtn.click(function() {
                $('.agreement-box').hide();
                $('#register_content').show();

                });


        $body.on("click", '.login-sub-btn', function (e) {
            e.preventDefault();
            var log = $("input[name=log]").val();
            var pwd = $("input[name=pwd]").val();
            $.ajax({
                method: 'POST',
                url: js_var.ajax_url,
                data: { action: "check_authentication", logname: log, pwd: pwd},
                dataType: "json",
                async:false,
                success: function (msg) {
                    if(msg.status == 2){
                        $('#login .alert-danger').text('Username or password not match!');
                        $('#login .alert-danger').removeClass('hidden');
                    }else if(msg.status == 1){
                        $('#loginFrm').submit();
                    }else if(msg.status == 3){
                        $('#login .alert-danger').text('Your account not approved yet!');
                        $('#login .alert-danger').removeClass('hidden');
                    }
                }
            });
        });
        $body.on("click", '.sign-sub-btn', function (e) {
            e.preventDefault();
            var user_login = $("input[name=user_login]").val();
            var company_name = $("input[name=company_name]").val();
            var user_email = $("input[name=user_email]").val();
            var pwd = $("input[name=password]").val();
            var g_recaptcha_response = grecaptcha.getResponse();
            $.ajax({
                method: 'POST',
                url: js_var.ajax_url,
                data: { action: "save_registration_data", user_login: user_login, company_name: company_name, user_email: user_email, pwd: pwd, g_recaptcha_response: g_recaptcha_response},
                dataType: "json",
                async:false,
                success: function (msg) {
                    if(msg.status == 2){
                        $('#register .alert-danger').text('Registration failed.Please try again.');
                        $('#register .alert-danger').removeClass('hidden');
                    }else if(msg.status == 1){
                        $('#register .alert-success').text('You have successfully completed registration!');
                        $('#register .alert-success').removeClass('hidden');
                        setTimeout(function(){
                            window.location.href = js_var.homeurl+'taycora-marketplace';
                        },3000);
                    }else if(msg.status == 3){
                        $('#register .alert-danger').text('User already exist with this name or email. Please try with different name and email.');
                        $('#register .alert-danger').removeClass('hidden');
                    }else if(msg.status == 4){
                        $('#register .alert-danger').text('Please check on the reCAPTCHA box.');
                        $('#register .alert-danger').removeClass('hidden');
                    }else if(msg.status == 5){
                        $('#register .alert-danger').text('Robot verification failed, please try again.');
                        $('#register .alert-danger').removeClass('hidden');
                    }
                }
            });
        });

        $body.on("click", '.showpopup', function (e) {
            e.preventDefault();
            $('#investment').css({'opacity':'1','visibility': 'visible'});
        });

        $('.invest-close').click(function() {
            $('#investment').css({'opacity':'0','visibility': 'hidden'})
        });

        $body.on("click", '.sub-for-inv', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var pid = $(this).attr('data-pid');
            var user_login = $("input[name=user_login2]").val();
            var company_name = $("input[name=company_name2]").val();
            var user_email = $("input[name=user_email2]").val();
            if($("input[name=password2]").length > 0){
                var pwd = $("input[name=password2]").val();
            }
            if( $('.form-group .checkbox #create_check').length > 0 ){
                if($('.form-group .checkbox #create_check').is(':checked')){
                    var g_recaptcha_response = grecaptcha.getResponse();
                    $.ajax({
                        method: 'POST',
                        url: js_var.ajax_url,
                        data: { action: "save_registration_data_send_email_with_agreement", user_login: user_login, company_name: company_name, user_email: user_email, pid: pid, pwd: pwd, g_recaptcha_response: g_recaptcha_response},
                        dataType: "json",
                        async:false,
                        cache: false,
                        success: function (msg) {
                            if(msg.status == 2){
                                $('#investment .alert-danger').text('Registration failed.Please try again.');
                                $('#investment .alert-danger').removeClass('hidden');
                            }else if(msg.status == 1){
                                $('#investment .alert-success').text('You have successfully completed registration!');
                                $('#investment .alert-success').removeClass('hidden');
                                setTimeout(function(){
                                    location.reload(true);
                                },3000);
                            }else if(msg.status == 3){
                                $('#investment .alert-danger').text('User already exist with this name or email. Please try with different name and email.');
                                $('#investment .alert-danger').removeClass('hidden');
                            }else if(msg.status == 4){
                                $('#investment .alert-success').text('Registration complete and a document is sent to your email. Please check!');
                                $('#investment .alert-success').removeClass('hidden');
                                setTimeout(function(){
                                    location.reload(true);
                                },3000);
                            }else if(msg.status == 6){
                                $('#investment .alert-danger').text('Please check on the reCAPTCHA box.');
                                $('#investment .alert-danger').removeClass('hidden');
                            }else if(msg.status == 5){
                                $('#investment .alert-danger').text('Robot verification failed, please try again.');
                                $('#investment .alert-danger').removeClass('hidden');
                            }
                        }
                    });
                }else{
                    var g_recaptcha_response = grecaptcha.getResponse();
                    $.ajax({
                        method: 'POST',
                        url: js_var.ajax_url,
                        data: { action: "send_email_with_agreement2", user_login: user_login, company_name: company_name, user_email: user_email, pid: pid, g_recaptcha_response: g_recaptcha_response},
                        dataType: "json",
                        async:false,
                        cache: false,
                        success: function (msg) {
                            if(msg.status == 2){
                                $('#investment .alert-danger').text('Document not send. Please try again.');
                                $('#investment .alert-danger').removeClass('hidden');
                            }else if(msg.status == 1){
                                $('#investment .alert-success').text('A document is sent to your email. Please check!');
                                $('#investment .alert-success').removeClass('hidden');
                                setTimeout(function(){
                                    location.reload(true);
                                },3000);
                            }else if(msg.status == 3){
                                $('#investment .alert-danger').text('Document not attached with this. Please try later!');
                                $('#investment .alert-danger').removeClass('hidden');
                            }else if(msg.status == 4){
                                $('#investment .alert-danger').text('Please check on the reCAPTCHA box.');
                                $('#investment .alert-danger').removeClass('hidden');
                            }else if(msg.status == 5){
                                $('#investment .alert-danger').text('Robot verification failed, please try again.');
                                $('#investment .alert-danger').removeClass('hidden');
                            }
                        }
                    });
                }
            }else{
                $.ajax({
                    method: 'POST',
                    url: js_var.ajax_url,
                    data: { action: "send_email_with_agreement", user_login: user_login, company_name: company_name, user_email: user_email, pid: pid},
                    dataType: "json",
                    async:false,
                    cache: false,
                    success: function (msg) {
                        if(msg.status == 2){
                            $('#investment .alert-danger').text('Document not send. Please try again.');
                            $('#investment .alert-danger').removeClass('hidden');
                        }else if(msg.status == 1){
                            $('#investment .alert-success').text('Document sent to your mail. Please check!');
                            $('#investment .alert-success').removeClass('hidden');
                            setTimeout(function(){
                                location.reload(true);
                            },3000);
                        }else if(msg.status == 3){
                            $('#investment .alert-danger').text('Document not attached with this. Please try later!');
                            $('#investment .alert-danger').removeClass('hidden');
                        }
                    }
                });
            }
        });

    });
})(jQuery, window, document);
